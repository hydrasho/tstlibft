#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <bsd/string.h>
#include <ctype.h>
#include "libft.h"

#define BFSIZE 50
#define STRKO "\x1b[31m[KO] "
#define STRISOK "\x1b[32m[OK] "
#define END "\n\x1b[37m"
#define ABS(a) a > 0 ? 0 : 1

int	main(void)
{
	char strKO[] = STRKO;

	printf("\n\x1b[34m[PARTIE 2] :\n\n");
	printf("\x1b[37m");


	/* FT_SUBSTR */
	printf("[FT_SUBSTR]");
	char *str;
	str= ft_substr("Hello world", 6, 5);
	if (strcmp(str, "world"))
		printf("%s ,SUBSTR valeur rendu: [%s] et attendu: [%s]", strKO, str, "world");
	else
		printf(STRISOK);
	free(str);
	str= ft_substr("Hello world", 0, 5);
	if (strcmp(str, "Hello"))
		printf("%s ,SUBSTR valeur rendu: [%s] et attendu: [%s]", strKO, str, "world");
	else
		printf(STRISOK);
	free(str);
	str= ft_substr("Hello world", 0, 420000);
	if (strcmp(str, "Hello world"))
		printf("%s ,SUBSTR taille trop grosse crash tout", strKO);
	else
		printf(STRISOK);
	free(str);
	str= ft_substr("Hola seniorita", 1, 1);
	if (strcmp(str, "o"))
		printf("%s ,SUBSTR taille trop grosse crash tout", strKO);
	else
		printf(STRISOK);
	free(str);
	str= ft_substr("1", 42, 42000000);
	if (strcmp(str, ""))
		printf("%s ,SUBSTR taille trop grosse crash tout", strKO);
	else
		printf(STRISOK);
	free(str);
	str= ft_substr("Hello world", 0, 0);
	if (strcmp(str, ""))
		printf("%s ,SUBSTR Ne fonctionne pas avec une chaine vide", strKO);
	else
		printf(STRISOK);
	free(str);
	str= ft_substr("Hello world", 15, 5);
	if (strcmp(str, ""))
		printf("%s ,SUBSTR depasse son tampon", strKO);
	else
		printf(STRISOK);
	free(str);
	str= ft_substr("Hello world", 0, 50);
	if (strcmp(str, "Hello world"))
		printf("%s ,SUBSTR depasse son tampon", strKO);
	else
		printf(STRISOK);
	free(str);
	str= ft_substr("", 25, 50);
	if (strcmp(str, ""))
		printf("%s ,SUBSTR depasse son tampon", strKO);
	else
		printf(STRISOK);
	free(str);
	printf(END);//END



	/* FT_STRJOIN */
	printf("[FT_STRJOIN]");
	str = ft_strjoin("salut, les amies ", "bonjours les amies");	
	if(strcmp(str, "salut, les amies bonjours les amies"))
		printf("%s ,SUBSTR valeur rendu: [%s] et attendu: [%s]", strKO, str, "world");
	else
		printf(STRISOK);
	free(str);
	str = ft_strjoin("salut, les amies ", "bonjours les amies");	
	if(strcmp(str, "salut, les amies bonjours les amies"))
		printf("%s ,SUBSTR valeur rendu: [%s] et attendu: [%s]", strKO, str, "salut, les amies bonjours les amies");
	else
		printf(STRISOK);
	free(str);
	str = ft_strjoin("s", "h");	
	if(strcmp(str, "sh"))
		printf("%s ,SUBSTR valeur rendu: [%s] et attendu: [%s]", strKO, str, "sh");
	else
		printf(STRISOK);
	free(str);
	str = ft_strjoin("", "bonjours les amies");	
	if(strcmp(str, "bonjours les amies"))
		printf("%s ,SUBSTR valeur rendu: [%s] et attendu: [%s]", strKO, str, "bonjours les amies");
	else
		printf(STRISOK);
	free(str); 
	str = ft_strjoin("", "");	
	if(strcmp(str, ""))
		printf("%s ,SUBSTR ne fonctionne pas avec des chaines vides", strKO);
	else
		printf(STRISOK);
	free(str); 
	str = ft_strjoin("x", "");	
	if(strcmp(str, "x"))
		printf("%s ,SUBSTR ne fonctionne pas avec des chaines vides", strKO);
	else
		printf(STRISOK);
	free(str); 
	printf(END);//END



	/* FT_STRTRIM*/
	printf("[FT_STRTRIM]");
	str = ft_strtrim("cuhellocu", "cu");
	if(strcmp(str, "hello"))
		printf("%s ,STRTRIM ne fonctionne pas du tout", strKO);
	else
		printf(STRISOK);
	free(str);
	str = ft_strtrim("cucucuhellocu", "cu");
	if(strcmp(str, "hello"))
		printf("%s ,STRTRIM ne fonctionne pas du tout", strKO);
	else
		printf(STRISOK);
	free(str);
	str = ft_strtrim("cuhellocuk", "cu");
	if(strcmp(str, "hellocuk"))
		printf("%s ,STRTRIM ne fonctionne pas du tout", strKO);
	else
		printf(STRISOK);
	free(str);
	str = ft_strtrim("", "cu");
	if(strcmp(str, ""))
		printf("%s ,STRTRIM ne fonctionne pas avec des chaines vides", strKO);
	else
		printf(STRISOK);
	free(str);
	str = ft_strtrim("cuhellocuk", "");
	if(strcmp(str, "cuhellocuk"))
		printf("%s ,STRTRIM ne fonctionne pas avec des chaines vides", strKO);
	else
		printf(STRISOK);
	free(str);
	str = ft_strtrim("", "");
	if(strcmp(str, ""))
		printf("%s ,STRTRIM ne fonctionne pas avec des chaines vides", strKO);
	else
		printf(STRISOK);
	free(str);
	str = ft_strtrim("   \n x\n\n\n\r\t\v    ", "\n\n\n\r\t\v    ");
	if(strcmp(str, "x"))
		printf("%s ,STRTRIM ne fonctionne pas avec des (ISSPACE)", strKO);
	else
		printf(STRISOK);
	free(str);
	str = ft_strtrim("\nhe\r\tll\no wo\v\vrld", "\n\r\t\v\v\v");
	if(strcmp(str, "he\r\tll\no wo\v\vrld"))
		printf("%s ,STRTRIM ne fonctionne pas avec des (ISSPACE)", strKO);
	else
		printf(STRISOK);
	free(str);

	printf("\e[0m");
	return (0);
}
