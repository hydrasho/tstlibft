#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include "libft.h"

#define STRKO "\x1b[31m[KO] "
#define STRISOK "\x1b[32m[OK] "
#define END "\n\x1b[37m"

int	main(void)
{
	char strKO[] = STRKO;
	char *str;
	

	printf("\n[FT_ITOA]");

	str = ft_itoa(INT_MAX);
	if (strcmp(str, "2147483647"))
		printf("%s ITOA ne gere pas le INTMAX", strKO);
	else
		printf(STRISOK);
	free(str);
	

	str = ft_itoa(INT_MIN);
	if (strcmp(str, "-2147483648"))
		printf("%s ITOA ne gere pas le INTMIN", strKO);
	else
		printf(STRISOK);
	free(str);
	
	str = ft_itoa(0);
	if (strcmp(str, "0"))
		printf("%s ITOA ne gere pas le 0", strKO);
	else
		printf(STRISOK);
	free(str);

	str = ft_itoa(1);
	if (strcmp(str, "1"))
		printf("%s ITOA ne gere pas le 1", strKO);
	else
		printf(STRISOK);
	free(str);
	

	str = ft_itoa(9);
	if (strcmp(str, "9"))
		printf("%s ITOA ne gere pas le 9", strKO);
	else
		printf(STRISOK);
	free(str);


	str = ft_itoa(10);
	if (strcmp(str, "10"))
		printf("%s ITOA ne gere pas le 10", strKO);
	else
		printf(STRISOK);
	free(str);


	str = ft_itoa(-1);
	if (strcmp(str, "-1"))
		printf("%s ITOA ne gere pas le -1", strKO);
	else
		printf(STRISOK);
	free(str);


	str = ft_itoa(-9);
	if (strcmp(str, "-9"))
		printf("%s ITOA ne gere pas le -9", strKO);
	else
		printf(STRISOK);
	free(str);
	
	str = ft_itoa(-10);
	if (strcmp(str, "-10"))
		printf("%s ITOA ne gere pas le -10", strKO);
	else
		printf(STRISOK);
	free(str);
	
	str = ft_itoa(42);
	if (strcmp(str, "42"))
		printf("%s ITOA ne gere pas le 42", strKO);
	else
		printf(STRISOK);
	free(str);
	
	str = ft_itoa(165468465);
	if (strcmp(str, "165468465"))
		printf("%s ITOA ne gere pas un nombre assez consequent ", strKO);
	else
		printf(STRISOK);
	free(str);

	printf(END);//END
	return (0);
}
