#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <bsd/string.h>
#include <ctype.h>
#include "libft.h"

#define BFSIZE 50
#define STRKO "\x1b[31m[KO] "
#define STRISOK "\x1b[32m[OK]"
#define END "\n\x1b[37m"

char strCAT1[60] = "KLJHR(854^&*90X0C9G6F7DIGSRD4;KfnbGH5DY745DGDAW43)";
char strCAT2[60] = "jksfdughffd54gf68d4g8d4gfd5fd4fd8h65dhf98d";

int ABS(int a){
	if(a > 0)
		return 1;
	else
		return 0;
}

int	main(void)
{
	char strKO[] = STRKO;
	//printf("\x1b[40m");//background
	printf("\n\n\n\x1b[34m ^^^^^^^^^^^^^^^^^^^	hydral-nda-cunh	^^^^^^^^^^^^^^^^^^^^^:\n");

	printf("[PARTIE 1] :\n\n");
	printf("\x1b[37m");
	
	/* Alpha */
	printf("[ISALPHA] ");
	for (int i = 45; i != 78; i++)
	{
		if(ABS(isalpha(i)) != ABS(ft_isalpha(i)))
			printf("%s, ISALPHA ['%d' != '%d']", strKO, ABS(ft_isalpha(i)), ABS(isalpha(i)));
		else
			printf(STRISOK);
	}
	printf(END);//END


	/* ID DIGIT */
	printf("[ISDIGIT] ");
	if (ABS(ft_isdigit('9')) == 0)
		printf(" %s ,is_digit lettre '9' envoie 0", strKO);
	else
		printf(STRISOK);
	if (ABS(ft_isdigit('0')) == 0)
		printf("%s ,is_digit lettre '0' envoie 0", strKO);
	else
		printf(STRISOK);
	for (int i = 45; i != 78; i++)
	{
		if(ABS(isdigit(i)) != ABS(ft_isdigit(i)))
			printf("%s, ISDIGIT ['%d' != '%d']", strKO, ABS(ft_isalpha(i)), ABS(isalpha(i)));
		else
			printf(STRISOK);
	}
	printf(END);//END


	/* ID ALNUM */
	printf("[ISALNUM] ");
	if(ABS(ft_isalnum('x')) != ABS(ft_isalnum('8')))
		printf("%s ,is_alnum KO 'x' et 'A'", strKO);
	else
		printf(STRISOK);
	for (int i = 0; i != 127; i++)
	{
		if(ABS(ft_isalnum(i)) != ABS(isalnum(i)))
			printf("%s ,is_alnum lettre '%c' envoie 0", strKO, i);
		else
			printf(STRISOK);
	}
	printf(END);//END


	/* IS ASCII */
	printf("[ISASCII] ");
	if(ABS(ft_isascii('x')) != ABS(isascii('x')))
		printf("%s ,is_alnum KO 'x' et 'A'", strKO);
	else
		printf(STRISOK);
	for (int i = -10; i != 25; i++)
	{
		if(ABS(isascii(i)) != ABS(ft_isascii(i)))
			printf("%s ,is_ascii lettre '%c' envoie 1", strKO, i);
		else
			printf(STRISOK);
	}
	for (int i = 100; i != 127; i++)
	{
		if(ABS(isascii(i)) != ABS(ft_isascii(i)))
			printf("%s ,is_ascii lettre '%c' envoie 1", strKO, i);
		else
			printf(STRISOK);
	}
	printf(END);//END


	/* IS PRINT */
	printf("[ISPRINT] ");
	for (int i = 5; i != 35; i++)
	{
		if(ABS(isprint(i)) != ABS(ft_isprint(i)))
			printf("%s ,[isprint:%d  ft_isprint:%d]", strKO, ABS(isprint(i)), ABS(ft_isprint(i)));
		else
			printf(STRISOK);
	}
	for (int i = 120; i != 127; i++)
	{
		if(ABS(isprint(i)) != ABS(ft_isprint(i)))
			printf("%s ,[isprint:%d  ft_isprint:%d]", strKO, ABS(isprint(i)), ABS(ft_isprint(i)));
		else
			printf(STRISOK);
	}
	printf(END);//END

	/* STRLEN */
	printf("[STRLEN] ");
	char str[42] = "Yolow c'est parfait";

	if(strlen(str) != ft_strlen(str))
		printf("%s ,STRLEN strlen:%ld ft_strlen:%ld", strKO, strlen(str), ft_strlen(str));
	else
		printf(STRISOK);
	if(strlen("") != ft_strlen(""))
		printf("%s ,STRLEN strlen:%ld ft_strlen:%ld", strKO, strlen(""), ft_strlen(""));
	else
		printf(STRISOK);
	if(strlen("BabyBoss") != ft_strlen("BabyBoss"))
		printf("%s ,STRLEN strlen:%ld ft_strlen:%ld", strKO, strlen("BabyBoss"), ft_strlen("BabyBoss"));
	else
		printf(STRISOK);
	if(strlen("   \t\t\t\r\n") != ft_strlen("   \t\t\t\r\n"))
		printf("%s ,STRLEN strlen:%ld ft_strlen:%ld", strKO, strlen("   \t\t\t\r\n"), ft_strlen("   \t\t\t\r\n"));
	else
		printf(STRISOK);
	printf(END);//END

	/* MEMSET */
	printf("[MEMSET] ");

	char str_memset[60];
	memset(str_memset, 'Y', 60);
	ft_memset(str_memset, 'x', 1);
	if (str_memset[0] != 'x')
		printf("%s, MEMSET KO [0]", strKO);
	else
		printf(STRISOK);
	if (str_memset[1] != 'Y')
		printf("%s, MEMSET KO , alloue trop loin", strKO);
	else
		printf(STRISOK);
	if (str_memset[2] == 'x')
		printf("%s, MEMSET KO , ??????????????????????", strKO);
	else
		printf(STRISOK);
	ft_memset(str_memset, 'U', 20);
	if (str_memset[15] != 'U')
		printf("%s, MEMSET KO [U]", strKO);
	else
		printf(STRISOK);

	char *destMEMSET = calloc(2500, sizeof(char));
	char *destMEMSET2 = calloc(2500, sizeof(char));

	memset(destMEMSET, 'U', 2500);
	memset(destMEMSET2, 'U', 2500);
	for(int i = 1; i != 2000; i++)
	{
		memset(destMEMSET2, '\5', i);
		ft_memset(destMEMSET, '\5', i);
		if (memcmp(destMEMSET, destMEMSET2, 2000))
		{
			printf("%s, MEMSET KO Teste de 25000", strKO);
			break;
		}
	}
	char    *b1 = (char*)malloc(sizeof(char) * (BFSIZE + 1));
	char    *b2 = (char*)malloc(sizeof(char) * (BFSIZE + 1));

	*b1 = 0;
	*b2 = 0;
	memset(b1, '\5', BFSIZE);
	ft_memset(b2, '\5', BFSIZE);

	if (memcmp(b1, b2, BFSIZE))
	{
		printf("%s, MEMSET KO Teste de 75000", strKO);
	}
	else
		printf(STRISOK);

	free(destMEMSET);
	free(destMEMSET2);
	free(b1);
	free(b2);

	printf(END);//END


	/* BZERO */
	printf("[BZERO] ");
	ft_bzero(str_memset, 55);
	for (int i = 0; i!= 55; i++)
	{
		if (str_memset[i] != '\0')
			printf("%s, BZERO KO %d", strKO, i);
	}
	if (str_memset[56] == '\0')
		printf("%s, BZERO KO %d", strKO, 56);
	else
		printf(STRISOK);
	printf(END);//END


	/* MEMCPY */
	printf("[MEMCPY] ");
	int memcpy_int1[] = {52, 86, 74};
	int memcpy_int2[3];
	ft_memcpy(memcpy_int2, memcpy_int1, sizeof(int) * 3);
	if (memcpy_int2[0] != 52)
		printf("%s, MEMCPY NE PEUT COPIER UN TABLEAU DE INT valeur: %d", strKO, memcpy_int2[0]);
	else
		printf(STRISOK);
	if (memcpy_int2[1] != 86)
		printf("%s, MEMCPY NE PEUT COPIER UN TABLEAU DE INT valeur: %d", strKO, memcpy_int2[0]);
	else
		printf(STRISOK);
	if (memcpy_int2[2] != 74)
		printf("%s, MEMCPY NE PEUT COPIER UN TABLEAU DE INT valeur: %d", strKO, memcpy_int2[0]);
	else
		printf(STRISOK);
	ft_memcpy(str_memset, "yolo ca va", 15);
	if (strcmp(str_memset, "yolo ca va") != 0)
		printf("%s, MEMCPY NE PEUT COPIER de chaine valeur: %d", strKO, memcpy_int2[0]);
	else
		printf(STRISOK);
	printf(END);//END

	/* MEMMOVE */
	printf("[MEMMOVE] ");

	char strMOVE[40] = "MAcumba danse tout les soirs";
	char strMOVE2[40] = "JIRAIS OU TU IRAS LA OU LE SOLEIL EST";

	char strMOVEX[40] = "MAcumba danse tout les soirs";
	char strMOVE2X[40] = "JIRAIS OU TU IRAS LA OU LE SOLEIL EST";

	char YstrMOVE2[40] = "JIRAIS OU TU IRAS LA OU LE SOLEIL EST";
	char YstrMOVE[40] = "MAcumba danse tout les soirs";

	char YstrMOVE2X[40] = "JIRAIS OU TU IRAS LA OU LE SOLEIL EST";
	char YstrMOVEX[40] = "MAcumba danse tout les soirs";

	if (strcmp(ft_memmove(strMOVE2, strMOVE, 20), memmove(strMOVE2X, strMOVEX, 20)))
		printf(" %s, MEMMOVE  ERREUR super horraire", strKO);
	else
		printf(STRISOK);
	if (strcmp(ft_memmove(YstrMOVE2, YstrMOVE, 15), memmove(YstrMOVE2X, YstrMOVEX, 15)))
		printf(" %s, MEMMOVE  ERREUR anti horraire", strKO);
	else
		printf(STRISOK);
	printf(END);//END



	/* STRLCPY */
	printf("[STRLCPY] ");
	if (ft_strlcpy(strMOVE2, strMOVE, 20) != strlcpy(strMOVE2X, strMOVEX, 20))
		printf(" %s, STRLCPY ERREUR", strKO);
	else
		printf(STRISOK);
	if (ft_strlcpy(YstrMOVE2, YstrMOVE, 8) != strlcpy(YstrMOVE2X, YstrMOVEX, 8))
		printf(" %s, STRLCPY ERREUR", strKO);
	else
		printf(STRISOK);
	if (ft_strlcpy(YstrMOVE2, "cest quoi ca tu es serieux ?", 25) != strlcpy(YstrMOVE2X, "cest quoi ca tu es serieux ?", 25))
		printf(" %s, STRLCPY ERREUR", strKO);
	else
		printf(STRISOK);
	char *strCPY;
	char *strCPY2;
	strCPY = ft_strdup("Hello");
	strCPY2 = ft_strdup("NASlo");
	if (ft_strlcpy(strCPY, strCPY2, 3) != strlcpy(strCPY, strCPY2, 3))
		printf(" %s, STRLCPY ERREUR", strKO);
	else
		printf(STRISOK);
	free(strCPY);
	free(strCPY2);
	printf(END);//END

	/* STRLCAT */

	char strCat[42] = "Lorem lipsum 1 2 3 4 5 6";
	char destCat[60] = "un cheval, des chevalidos";

	char strCatX[42] = "Lorem lipsum 1 2 3 4 5 6";
	char destCatX[60] = "un cheval, des chevalidos";

	printf("[STRLCAT] ");
	if (strlcat(destCat, "", 50) != ft_strlcat(destCatX, "", 50))	
		printf(" %s, STRLCAT mauvaise size", strKO);
	else
		printf(STRISOK);
	if (strcmp(destCat, destCatX) != 0)	
		printf(" %s, STRLCAT ne fonctionne pas avec une chaine vide.\n", strKO);
	else
		printf(STRISOK);
	if (strlcat(destCat, strCat, 50) != ft_strlcat(destCatX, strCatX, 50))	
		printf(" %s, STRLCAT mauvaise size", strKO);
	else
		printf(STRISOK);
	if (strcmp(destCat, destCatX) != 0)	
		printf(" %s, STRLCAT ne fonctionne pas du tout.", strKO);
	else
		printf(STRISOK);
	if (strlcat(destCat, strCat, 20) != ft_strlcat(destCatX, strCatX, 20))	
		printf(" %s, STRLCAT KO", strKO);
	else
		printf(STRISOK);

	printf(END);//END


	/* TOUPPER */
	printf("[TOUPPER] ");
	if((toupper('A')) != ft_toupper('A'))
			printf(" %s ,TOUPPER '%c' et '%c'", strKO, toupper('A'), ft_toupper('A'));
	else
		printf(STRISOK);
	for (int i = 0; i != 195; i++)
	{
		if((toupper(i)) != ft_toupper(i))
			printf(" %s ,TOUPPER '%c' et '%c'", strKO, toupper(i), ft_toupper(i));
	}
	printf(END);//END

	/* TOLOWER */
	printf("[TOLOWER] ");
	if((tolower('A')) != ft_tolower('A'))
			printf(" %s ,TOUPPER '%c' et '%c'", strKO, toupper('A'), ft_toupper('A'));
	else
		printf(STRISOK);
	for (int i = 0; i != 195; i++)
	{
		if((tolower(i)) != ft_tolower(i))
			printf(" %s ,TOLOWER '%c' et '%c'", strKO, tolower(i), ft_tolower(i));
	}
	printf(END);//END

	/* STRCHR */
	printf("[STRCHR] ");//START
	char schr[] = "tripouillexxx";
	if (ft_strchr(schr, 'i') != &schr[2])
		printf(" %s, STRCHR", strKO);
	else
		printf(STRISOK);
	if (ft_strchr(schr, 'l') != &schr[7])
		printf(" %s, STRCHR", strKO);
	else
		printf(STRISOK);
	if (ft_strchr(schr, '&') != NULL)
		printf(" %s, STRCHR", strKO);
	else
		printf(STRISOK);
	if (ft_strchr(schr, 't') != &schr[0])
		printf(" %s, STRCHR", strKO);
	else
		printf(STRISOK);
	if (ft_strchr(schr, 'p') != &schr[3])
		printf(" %s, STRCHR", strKO);
	else
		printf(STRISOK);
	printf(END);//END


	/* STRRCHR */
	printf("[STRRCHR] ");//START
	if (ft_strrchr(schr, 'i') != &schr[6])
		printf(" %s, STRRCHR", strKO);
	else
		printf(STRISOK);
	if (ft_strrchr(schr, 'Y') != NULL)
		printf(" %s, STRRCHR", strKO);
	else
		printf(STRISOK);
	if (ft_strrchr(schr, 'l') != &schr[8])
		printf(" %s, STRRCHR", strKO);
	else
		printf(STRISOK);
	if (ft_strrchr(schr, 't') != &schr[0])
		printf(" %s, STRRCHR", strKO);
	else
		printf(STRISOK);

	printf(END);//END

	/* STRNCMP */
	printf("[STRNCMP] ");//START
	if (ft_strncmp("bjru", "bjrd", 3) != strncmp("bjru", "bjrd", 3))
		printf(" %s,STRNCMP simple envoie", strKO);
	else
		printf(STRISOK);
	if (ft_strncmp("bjr\0kitty", "bjr\0hello", 7) != strncmp("bjr\0kitty", "bjr\0hello", 7))
		printf(" %s,STRNCMP Depassement de tampon", strKO);
	else
		printf(STRISOK);
	if (ft_strncmp("test\200", "test\0", 6) != strncmp("test\200", "test\0", 6))
		printf("\x1b[33m,[WARNING]STRNCMP ne gere pas le unsigned char");
	else
		printf(STRISOK);
	if (ft_strncmp(str, str, 10) != strncmp(str, str, 10))
		printf(" %s,STRNCMP simple envoie", strKO);
	else
		printf(STRISOK);
	if (ft_strncmp(strCatX, strCat, 25) != strncmp(strCatX, strCat, 25))
		printf(" %s,STRNCMP plus fort envoie", strKO);
	else
		printf(STRISOK);
	if (ft_strncmp("", "", 5) != strncmp("", "", 5))
		printf(" %s,STRNCMP ne comparre pas les chaines vides", strKO);
	else
		printf(STRISOK);
	if (ft_strncmp("fhfghfgdjhsffg", "dfghfdhsfd", 5) != strncmp("fhfghfgdjhsffg", "dfghfdhsfd", 5))
		printf(" %s,STRNCMP comparaison de [fhfghfgdjhsffg et dfghfdhsfd] à [5] votre valeur: %d valeur libc %d", strKO, strncmp("fhfghfgdjhsffg", "dfghfdhsfd", 5), ft_strncmp("fhfghfgdjhsffg", "dfghfdhsfd", 5));
	else
		printf(STRISOK);
	printf(END);//END


	/* MEMCHR */
	printf("[MEMCHR] ");//START
	char *smemchr = NULL;
	smemchr = strdup("egarer dans la valee infernal a la recherche de lambre jaune fdjigjfiods gjsf");
	if(ft_memchr(smemchr, 'e', 5) != &smemchr[0])
		printf(" %s, MEMCHR KO  simple envoie", strKO);
	else
		printf(STRISOK);
	int tabchr[] = {42, 87, -1, 86};
	if(ft_memchr(tabchr, -1, 16) != &tabchr[2])
		printf(" %s, MEMCHR KO  (unsigned char)", strKO);
	else
		printf(STRISOK);
	if(memchr(smemchr, 'a', 17) != ft_memchr(smemchr, 'a', 17))
		printf(" %s, MEMCHR KO ne fonctionne pas en dehors du 0", strKO);
	else
		printf(STRISOK);
	if(memchr(smemchr, 'x', 17) != ft_memchr(smemchr, 'x', 17))
		printf(" %s, MEMCHR KO ne fonctionne pas en dehors du 0", strKO);
	else
		printf(STRISOK);
	if(memchr("Romeo kiff juliette\0 juliette kiff romeo", 'x', 39) != ft_memchr("Romeo kiff juliette\0 juliette Ukiff romeo", 'x', 39))
		printf(" %s, MEMCHR KO MEMCHR s'arrete au \\0 !", strKO);
	else
		printf(STRISOK);
	if(memchr(smemchr, 'n', 45) != ft_memchr(smemchr, 'n', 45))
		printf(" %s, MEMCHR KO ne fonctionne pas en dehors du 0", strKO);
	else
		printf(STRISOK);
	if(memchr(smemchr, 'u', 40) != ft_memchr(smemchr, 'u', 40))
		printf(" %s, MEMCHR KO ne fonctionne pas en dehors du 0", strKO);
	else
		printf(STRISOK);
	if(memchr(smemchr, 'e', 0) != ft_memchr(smemchr, 'u', 0))
		printf(" %s, MEMCHR KO tu cherche trop tot !", strKO);
	else
		printf(STRISOK);
	printf(END);//END
	free(smemchr);


	/* MEMCMP */
	int aCMP[15] = {25, -1, 0, 58, 64, 25, 25, 8, 9, 7, -8};
	int bCMP[15] = {25, -1, 0, 58, 25, 24, 15, 999, 60 ,-1};
	int cCMP[2] = {25, 130};
	printf("[MEMCMP] ");//START

	if(ft_memcmp(aCMP, bCMP, 0) != memcmp(aCMP, bCMP, 0))
		printf(" %s, MEMCMP tu gere pas le n=0!", strKO);
	else
		printf(STRISOK);
	if(ft_memcmp(aCMP, bCMP, sizeof(int) != memcmp(aCMP, bCMP, sizeof(int))))
		printf(" %s, MEMCMP ne fonctionne meme pas !", strKO);
	else
		printf(STRISOK);
	if(ft_memcmp(aCMP, bCMP, sizeof(int) * 2 != memcmp(aCMP, bCMP, sizeof(int) * 2)))
		printf(" %s, MEMCMP ne fonctionne meme pas !", strKO);
	else
		printf(STRISOK);
	if(ft_memcmp(aCMP, bCMP, sizeof(int) * 3 != memcmp(aCMP, bCMP, sizeof(int) * 3)))
		printf(" %s, MEMCMP ne fonctionne meme pas !", strKO);
	else
		printf(STRISOK);
	if(ft_memcmp(aCMP, bCMP, sizeof(int) * 5 != memcmp(aCMP, bCMP, sizeof(int) * 5)))
		printf(" %s, MEMCMP gere le \\0", strKO);
	else
		printf(STRISOK);
	if(ft_memcmp("", "", sizeof(char) != memcmp("", "", sizeof(char))))
		printf(" %s, Ouhlala tu fais que des betises", strKO);
	else
		printf(STRISOK);
	if(ft_memcmp(aCMP, cCMP, sizeof(int) * 2 != memcmp(aCMP, cCMP, sizeof(int) * 2)))
		printf(" %s, MEMCMP Unisgnedchar!", strKO);//TODO rajouter unsigned char test
	else
		printf(STRISOK);
	printf(END);//END


	/* STRNSTR */
	printf("[STRNSTR] ");//START
	char strNSTR[] = "nvidia, fuck you ! mais 42 qu'a tu fais ici pourquoi nvid\0ia ? dis moi ! si je dois partir ou pas";
	if (ft_strnstr(strNSTR, "42", 50) != strnstr(strNSTR, "42", 50))
		printf(" %s, STRNSTR ne fonctionne pas", strKO);
	else
		printf(STRISOK);
	if (ft_strnstr(strNSTR, "42", 65) != strnstr(strNSTR, "42", 65))
		printf(" %s, STRNSTR ne fonctionne pas", strKO);
	else
		printf(STRISOK);
	if (ft_strnstr(strNSTR, "si", 75) != strnstr(strNSTR, "si", 75))
		printf(" %s, STRNSTR depasse le tampon !", strKO);
	else
		printf(STRISOK);
	if (ft_strnstr(strNSTR, "si", 0) != strnstr(strNSTR, "si", 0))
		printf(" %s, STRNSTR Attention aux N=0 !", strKO);
	else
		printf(STRISOK);
	if (ft_strnstr(strNSTR, "ABCDEFGHIJKLMNOP", 70) != strnstr(strNSTR, "ABCDEFGHIJKLMNOP", 70))
		printf(" %s, Il aime pas trop les grosse chaines oO", strKO);
	else
		printf(STRISOK);
	if (ft_strnstr(strNSTR, "", 70) != strnstr(strNSTR, "", 70))
		printf(" %s, Les chaines vide ca fait de sacré betise !", strKO);
	else
		printf(STRISOK);
	if (ft_strnstr("", "yolo", 70) != strnstr("", "yolo", 70))
		printf(" %s, Les chaines vide ca fait de sacré betise !", strKO);
	else
		printf(STRISOK);
	if (ft_strnstr("", "", 70) != strnstr("", "", 70))
		printf(" %s, Les chaines vide ca fait de sacré betise !", strKO);
	else
		printf(STRISOK);
	if (ft_strnstr(NULL, "", 70) != strnstr(NULL, "", 70))
		printf(" %s, Tu ne protege pas le BIG !", strKO);
	else
		printf(STRISOK);
	printf(END);//END

	/* ATOI */
	printf("[ATOI] ");//START
	if (ft_atoi("42") != atoi("42"))
		printf(" %s, Atoi simple valeur [42] !", strKO);
	else
		printf(STRISOK);
	if (ft_atoi("9") != atoi("9"))
		printf(" %s, Atoi simple valeur [9] !", strKO);
	else
		printf(STRISOK);
	if (ft_atoi("0") != atoi("0"))
		printf(" %s, Atoi simple valeur [0] !", strKO);
	else
		printf(STRISOK);
	if (ft_atoi("1") != atoi("1"))
		printf(" %s, Atoi simple valeur [1] !", strKO);
	else
		printf(STRISOK);
	if (ft_atoi("-9") != atoi("-9"))
		printf(" %s, Atoi simple valeur [-9] !", strKO);
	else
		printf(STRISOK);
	if (ft_atoi("-10") != atoi("-10"))
		printf(" %s, Atoi simple valeur [-10] !", strKO);
	else
		printf(STRISOK);
	if (ft_atoi("10") != atoi("10"))
		printf(" %s, Atoi simple valeur [10] !", strKO);
	else
		printf(STRISOK);
	if (ft_atoi("2147483647") != atoi("2147483647"))
		printf(" %s, Atoi INT MAX!", strKO);
	else
		printf(STRISOK);
	if (ft_atoi("-2147483648") != atoi("-2147483648"))
		printf(" %s, Atoi INT MIN !", strKO);
	else
		printf(STRISOK);
	if (ft_atoi("   \t\r\f\v\t-2145") != atoi("   \t\f\r\v\t-2145"))
		printf(" %s, Atoi (ISSPACE) non gerer !", strKO);
	else
		printf(STRISOK);
	if (ft_atoi("   \t\t--2145") != atoi("   \t\t--2145"))
		printf(" %s, Atoi Il ne faut pas gerer les multiple signes !", strKO);
	else
		printf(STRISOK);
	if (ft_atoi("   \t\t-a2145") != atoi("   \t\t-a2145"))
		printf(" %s, Atoi pas de lettres !", strKO);
	else
		printf(STRISOK);
	if (ft_atoi("   \t\t-8a2145") != atoi("   \t\t-8a2145"))
		printf(" %s, Atoi s'arrete mal face aux autres caractere!", strKO);
	else
		printf(STRISOK);
	if (ft_atoi("   \n 28fkldjgd42") != atoi("   \n 28fkldjgd42"))
		printf(" %s, Atoi s'arrete mal face aux autres caractere!", strKO);
	else
		printf(STRISOK);
	if (ft_atoi("   \n\f\r\n\t\v") != atoi("   \n\f\r\n\t\v"))
		printf(" %s, Atoi Man (ISSPACE)", strKO);
	else
		printf(STRISOK);

	printf(END);//END

	/* Calloc */
	printf("[CALLOC] ");//START
	printf("\x1b[33m,[:)] NO CALLOC TESTE");
	printf(END);//END
	
	/* Strdup */
	printf("[STRDUP] ");//START
	char *strX = ft_strdup("jirais ou tu iras");
	char *strY = ft_strdup("jirais ou tu iras");
	if (strcmp(strX, strY))
		printf(" %s, STRDUP simple chaine!", strKO);
	else
		printf(STRISOK);
	free(strX);
	free(strY);
	strX = ft_strdup("");
	strY = ft_strdup("");
	if (strcmp(strX, strY))
		printf(" %s, STRDUP simple chaine!", strKO);
	else
		printf(STRISOK);
	free(strX);
	free(strY);
	printf(END);//END
	return (0);
}
