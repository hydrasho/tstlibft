#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "libft.h"

#define STRKO "\x1b[31m[KO] "
#define STRISOK "\x1b[32m[OK] "
#define END "\n\x1b[37m"

int	main(void)
{
	char strKO[] = STRKO;
	char **strtab;
	

	printf("\n[FT_SPLIT]");
	strtab = ft_split("", ',');
	if(strtab[0] != NULL)
		printf("%s SPLIT chaine vide avec caractere ne fonctionne pas ", strKO);
	else
		printf(STRISOK);
	for(int i = 0; strtab[i] != NULL; i++)
		free(strtab[i]);
	free(strtab);


	strtab = ft_split("yolo\0yolo", '\0');
	if(strtab[0] == NULL)
		printf("%s SPLIT ne fonctionne pas avec '\\0' en caractere", strKO);
	else if(strcmp(strtab[0], "yolo"))
		printf("%s SPLIT chaine vide avec caractere ne fonctionne pas ", strKO);
	else
		printf(STRISOK);
	for(int i = 0; strtab[i] != NULL; i++)
		free(strtab[i]);
	free(strtab);




	strtab = ft_split("yolo,yolo", ',');
	if(strtab[0] == NULL)
		printf("%s SPLIT ne fonction", strKO);
	else if(strcmp(strtab[0], "yolo") && strcmp(strtab[1], "yolo"))
		printf("%s SPLIT simple chaine ", strKO);
	else
		printf(STRISOK);
	for(int i = 0; strtab[i] != NULL; i++)
		free(strtab[i]);
	free(strtab);




	strtab = ft_split("yoloyolo", ',');
	if(strtab[0] == NULL)
		printf("%s SPLIT ne fonction", strKO);
	else if(strcmp(strtab[0], "yoloyolo"))
		printf("%s SPLIT sans caractere a tester ", strKO);
	else
		printf(STRISOK);
	for(int i = 0; strtab[i] != NULL; i++)
		free(strtab[i]);
	free(strtab);




	strtab = ft_split("yolo,", ',');
	if(strtab[0] == NULL)
		printf("%s SPLIT ne fonction", strKO);
	else if(strcmp(strtab[0], "yolo") && strcmp(strtab[1], ""))
		printf("%s SPLIT separateur a la fin sans rien dautre ", strKO);
	else
		printf(STRISOK);
	for(int i = 0; strtab[i] != NULL; i++)
		free(strtab[i]);
	free(strtab);



	strtab = ft_split("yolo,bonjour,salut,lol", ',');
	if(strtab[0] == NULL)
		printf("%s SPLIT ne fonction", strKO);
	else if(strcmp(strtab[0], "yolo"))
		printf("%s SPLIT separateur a la fin sans rien dautre ", strKO);
	else if(strcmp(strtab[1], "bonjour"))
		printf("%s SPLIT separateur a la fin sans rien dautre ", strKO);
	else if(strcmp(strtab[2], "salut"))
		printf("%s SPLIT separateur a la fin sans rien dautre ", strKO);
	else if(strcmp(strtab[3], "lol"))
		printf("%s SPLIT separateur a la fin sans rien dautre ", strKO);
	else if(strtab[4] != NULL)
		printf("%s SPLIT separateur a la fin sans rien dautre ", strKO);
	else
		printf(STRISOK);
	for(int i = 0; strtab[i] != NULL; i++)
		free(strtab[i]);
	free(strtab);




	char * invalidReadCheck = malloc(sizeof(char));
	*invalidReadCheck = 0;
	strtab = ft_split(invalidReadCheck, 0);
	if(strtab[0] != NULL)
		printf("%s SPLIT ne gere pas un caractere invalide", strKO);
	else
		printf(STRISOK);
	free(strtab[0]);
	free(strtab);
	free(invalidReadCheck);





	strtab = ft_split("--0--1---2--3-4------42----------", '-');
	if(strcmp(strtab[0], "0"))
		printf("%s SPLIT ne fonction tout simplement pas", strKO);
	else
		printf(STRISOK);
	if(strcmp(strtab[1], "1"))
		printf("%s SPLIT ne fonction tout simplement pas", strKO);
	else
		printf(STRISOK);
	if(strcmp(strtab[2], "2"))
		printf("%s SPLIT ne fonction tout simplement pas", strKO);
	else
		printf(STRISOK);
	if(strcmp(strtab[3], "3"))
		printf("%s SPLIT ne fonction tout simplement pas", strKO);
	else
		printf(STRISOK);
	if(strcmp(strtab[4], "4"))
		printf("%s SPLIT ne fonction tout simplement pas", strKO);
	else
		printf(STRISOK);
	if(strcmp(strtab[5], "42"))
		printf("%s SPLIT ne fonction tout simplement pas", strKO);
	else
		printf(STRISOK);
	if(strtab[6] != NULL)
		printf("%s SPLIT ne fonction tout simplement pas", strKO);
	else
		printf(STRISOK);
	for(int i = 0; strtab[i] != NULL; i++)
		free(strtab[i]);
	free(strtab);



	printf(END);//END
	return (0);
}
