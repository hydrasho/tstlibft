curl https://gitlab.com/hydrasho/tstlibft/-/raw/master/testeur.c > testeur.c
curl https://gitlab.com/hydrasho/tstlibft/-/raw/master/testeur2.c > testeur2.c
curl https://gitlab.com/hydrasho/tstlibft/-/raw/master/split.c > testesplit.c
curl https://gitlab.com/hydrasho/tstlibft/-/raw/master/itoa.c > testeitoa.c
make re
gcc testeur.c -Wall -Wextra -Werror -lbsd -L . -lft -o test.elf
gcc testeur2.c -Wall -Wextra -Werror -lbsd -L . -lft -o test2.elf
gcc testesplit.c -Wall -Wextra -Werror -lbsd -L . -lft -o testsp.elf
gcc testeitoa.c -Wall -Wextra -Werror -lbsd -L . -lft -o testitoa.elf
./test.elf
./test2.elf
./testsp.elf
./testitoa.elf
rm -f test.elf
rm -f testeur.c
rm -f testesplit.c
rm -f testeitoa.c
rm -f test2.elf
rm -f testsp.elf
rm -f testeur2.c
rm -f testitoa.elf